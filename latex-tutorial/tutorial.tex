\documentclass{elsarticle}

\usepackage{graphicx}
\graphicspath{ {../latex-tutorial/figs/} }
\usepackage{amsmath}
\usepackage[margin=1in]{geometry}

\begin{document}

% title
\title{Title}
\author{Adam Sierakowski}
\date{\today}
\maketitle

\tableofcontents

% document

% sections
%\chapter{Chapter}
\section{Section}
\subsection{Subsection}
\subsubsection{Subsubsection}
\paragraph{Paragraph}

% emphasis
\textbf{bold text}

\textit{italicized text}

\underline{underlined text}

\emph{emphasized text}

% graphics
\includegraphics{plot.png}

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\textwidth]{plot.png}
  \caption{our figure}
  \label{fig:image}
\end{figure}

We can reference our figure, Figure \ref{fig:image}, anywhere in the text.

% unordered lists
\begin{itemize}
  \item these
  \item are
  \item bulleted
  \item lists
  \begin{itemize}
    \item they can be
    \begin{itemize}
      \item arbitrarily nested
    \end{itemize}
  \end{itemize}
\end{itemize}

% ordered lists
\begin{enumerate}
  \item these
  \item are
  \item numbered
  \item lists
  \begin{enumerate}
    \item they can be
    \begin{enumerate}
      \item arbitrarily nested
    \end{enumerate}
  \end{enumerate}
\end{enumerate}

% math
You can place equations inline, such as $y = mx + b$.

You can also make them stand out more:
$$ m = \left\{ \frac{y - b}{x} \right\} $$

You can easily reference your equations:
\begin{equation}
  \frac{\partial \mathbf{u}}{\partial t} + \left(\mathbf{u}\cdot\nabla\right)\mathbf{u} = -\frac{1}{\rho}\nabla p + \nu\nabla^2\mathbf{u}
\label{eq:ns}
\end{equation}

See equation \ref{eq:ns}.

And everything looks nice with minimal effort:
\begin{equation}
  V=\int_0^{2\pi}\int_0^\pi\int_0^R r^2\sin\phi\sin\theta\ dr\,d\phi\,d\theta
\end{equation}

% tables
\begin{table}[ht]
  \centering
  \begin{tabular}{c|r|l}
    header 1 & header 2 & header 3 \\
    \hline
    a & b & c \\
    1 & 2 & 3
  \end{tabular}
  \caption{our table}
  \label{tab:our-table}
\end{table}

\end{document}
