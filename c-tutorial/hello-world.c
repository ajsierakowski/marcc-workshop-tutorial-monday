#include <stdlib.h>
#include <stdio.h>

/* compile with:
 * gcc -std=c99 -o hello-world hello-world.c
 */

int main(int argc, char *argv[])
{
  printf("Hello world!\n");

  // parse the command line arguments
  // i++ <==> i = i + 1
  printf("There are %d command line arguments.\n", argc);
  printf("They are:\n");
  for(int i = 0; i < argc; i++) {
    printf("  %d: %s\n", i, argv[i]);
  }

  // static array
  int A[10];

  // fill and print
  for(int i = 0; i < 10; i++) {
    A[i] = i;
    printf("A[%d] = %d\n", i, A[i]);
  }

  // variable array
  int B[argc];

  // fill and print
  for(int i = 0; i < argc; i++) {
    B[i] = i;
    printf("B[%d] = %d\n", i, B[i]);
  }

  // dynamic array
  int *C; // declaring that we have a memory location that will contain integers
  if(argc > 1) { // error checking
    int arglen = atoi(argv[1]);
    
    // allocate C
    C = malloc(arglen * sizeof(int));

    // fill and print C
    for(int i = 0; i < arglen; i++) {
      C[i] = i;
      printf("C[%d] = %d\n", i, C[i]);
    }

    // release memory of C
    free(C);
  }

  // return an integer defined by stdlib
  return EXIT_SUCCESS;
}
