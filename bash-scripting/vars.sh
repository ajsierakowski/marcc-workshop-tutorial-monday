#!/bin/bash

a=Hello
b=World

echo $a $b

dir=/home-1/class-2@jhu.edu

ls $dir

# single quotes are literal
c='Hello World'
echo $c

# double quotes substitute variables
d="Big $c"
echo $d

# save contents of a command into a variable
e=$(ls $dir | wc -l)
echo $e
