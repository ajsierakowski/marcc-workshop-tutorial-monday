#!/bin/bash -l

#SBATCH --job-name=tutorial
#SBATCH --time=00:10:00
#SBATCH --partition=debug
#SBATCH --nodes=1
# number of tasks (processes) per node
#SBATCH --ntasks-per-node=1
# number of cpus (threads) per task (process)
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=all
#SBATCH --mail-user=sierakowski@jhu.edu
#SBATCH --output=hello-job.out

#### load and unload modules you may need
module load matlab
module list

#### execute code and write output file to OUT-24log.
hello.sh
echo "Finished with job $SLURM_JOBID"

#### mpiexec by default launches number of tasks requested
