#!/usr/bin/env python3

L = ["JHU", 1876, None]
print(L[2])
print(L[2] == True)
L[2] = True
print(L[2])
print(L)
print(L.pop(2))
print(L)
L.append(False)
print(L)
L[1] = "Homewood"
L[2] = "Baltimore"
print(L)
L.sort()
print(L)
