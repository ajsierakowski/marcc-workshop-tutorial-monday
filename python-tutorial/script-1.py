#!/usr/bin/env python3

print(123 + 456)

print(1.5 * 4)

# below is integer division
print(1 / 2)

# below is floating point division
print(1. / 2.)

print(2 ** 8)
