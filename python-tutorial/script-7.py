#!/usr/bin/env python3

##################
# Define functions
##################

# define the input query
def get_input():
  return input("Type something: ")

# determine whether this is a single character or a string
def check_length():
  if len(user_in) < 1:
    print("  Empty")
  elif len(user_in) < 2:
    print("  Character")
  else:
    print("  String")

##############
# Begin script
##############

# get user input
user_in = get_input()

inputs = list()

# repeat until a "q" appears
while user_in.find("q") == -1:
  print(user_in)

  inputs.append(user_in)

  check_length()

  # print each character
  for i in range(len(user_in)):
    print("    user_in[" + str(i) + "] = " + user_in[i])

  # print each character (iterator)
  for char in user_in:
    print("    " + char)
    # change ending character:
    #print("    " + char, end='hi\n')
  
  # request another input
  user_in = get_input()

# notify user that loop has ended
print("Quit")
